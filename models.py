from sqlalchemy import Column, Integer, String, Numeric, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

class Store(Base):
	__tablename__ = 'Store'
	id = Column(Integer, primary_key=True)
	
	name = Column(String, nullable=False)


class Reciept(Base):
	__tablename__ = 'Reciept'
	id = Column(Integer, primary_key=True)
	
	store_id = Column(Integer, ForeignKey(Store.id), nullable=False)
	store = relationship(Store)


class PurchaseUnit(Base):
	__tablename__ = 'PurchaseUnit'
	
	id = Column(Integer, primary_key=True)
	
	name = Column(String, nullable=False)


class Purchase(Base):
	__tablename__ = 'Purchase'
	
	id = Column(Integer, primary_key=True)
	
	reciept_id = Column(Integer, ForeignKey('Reciept.id'),
		nullable=False)
	reciept = relationship("Reciept")
	
	description = Column(String, nullable=False)
	
	qty = Column(String, nullable=False)
	
	cost = Column(Integer, nullable=False)
	
	unit_id = Column(Integer, ForeignKey('PurchaseUnit.id'), nullable=False)
	unit = relationship("PurchaseUnit")
	
	def __repr__(self):
		return "{qty} {desc} @ {cost} [id: {id}, r: {rid}]".format(
			qty=self.qty, 
			desc=self.description, 
			cost=self.cost,
			id=self.id,
			rid=self.reciept)
