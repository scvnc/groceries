import unittest
from unittest.mock import patch
from repository import create_session

class Tests(unittest.TestCase):
	
	def test_that_create_session_works(self):
		session = create_session()
		
		self.assertIsNotNone(session)

###

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker




from models import Store, Reciept, PurchaseUnit, Purchase
from models import Base

class DatabaseTestCase(unittest.TestCase):
    
    def setUp(self):
        self.memory_engine = create_engine('sqlite:///:memory:')

        Base.metadata.create_all(self.memory_engine)

        self.db = sessionmaker(bind=self.memory_engine)()
        
        self.config_call_mock()
    
    def tearDown(self):
        self.db.close()
        del self.memory_engine
        
    def config_call_mock(self):

        callPatch = patch('services.create_session')
        self.callMock = callPatch.start()

        self.callMock.return_value = self.db

        self.addCleanup(callPatch.stop)

class ModelTests(DatabaseTestCase):


    ###

    def test_store(self):
        store = Store(name="Apple Market")
        
        self.db.add(store)
        self.db.commit()

    def test_reciept(self):
        
        store = Store(name="Apple Market")
        reciept = Reciept(store=store)
        
        self.db.add(reciept)
        self.db.commit()

    def test_PurchaseUnit(self):
        unit = PurchaseUnit(name="Oz")
        
        self.db.add(unit)
        self.db.commit()

    def test_purchase(self):
        
        store = Store(name="Apple Market")
        reciept = Reciept(store=store)
        unit = PurchaseUnit(name="Oz")
        
        purchase = Purchase(
            reciept=reciept,
            description="Soap",
            qty=3,
            cost=3.4,
            unit=unit)

        self.db.add(purchase)
        self.db.commit()


###

from services import StoreService
from sqlalchemy import func

class StoreServiceTests(DatabaseTestCase):
    
    testStoreName = "hookey"
    
    def setUp(self):
        super().setUp()
        self.store_svc = StoreService()
        
        s = Store(name=self.testStoreName)
        self.db.add(s)
        self.db.commit()
        
    def test_with_existing_store(self):
        
        result = self.store_svc.establish_store(self.testStoreName)
        
        # Verify that a Store was returned.
        self.assertIsNotNone(result)
        
        # Verify that no records were added
        self.assertEqual(self.db.query(Store).count(), 1)
        
    def test_with_nonexisting_store(self):
        
        self.db.query(func.count(Store))
        
        result = self.store_svc.establish_store("nodb")
        
        # Verify that the store was returned.
        self.assertIsNotNone(result)
        
        # Verify that the store was created.
        newStoreInDb = self.db.query(Store).filter(
            Store.name == "nodb").first()
            
        self.assertIsNotNone(newStoreInDb)

##

class CliTests(unittest.TestCase):
    
    def setUp(self):
        
        

if __name__ == '__main__':
    unittest.main()
