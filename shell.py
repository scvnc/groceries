import shlex
import cmd

from repository import create_session

from models import Store




store_svc = StoreService()

class MyCmd(cmd.Cmd):
	
	@property
	def prompt(self):
		return "grocery> "
	
	def completedefault(self, text, line, begidx, endidx):
		print (text)
		
	def do_reciept(self, store):
		
		store = store_svc.establish_store(store)
		
		print (store.name)
		
	def complete_reciept(self, text, line, begidx, endidx):
		
		pass



if __name__ == "__main__":
	myCmd = MyCmd()

	myCmd.cmdloop()
