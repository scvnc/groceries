from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

__engine = create_engine('sqlite:////home/vincent/doop.sqlite')
__Session = sessionmaker(bind=__engine)

def create_session():
    return __Session()
    
