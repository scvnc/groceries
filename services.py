from repository import create_session

from models import Store

class StoreService():
	
	def establish_store(self, name):
		
		db = create_session()
		
		store = db.query(Store).filter(Store.name == name).first()

		if not store:
			store = Store(name=name)
			db.add(store)
			db.commit()
		
		db.close()
		
		return store
