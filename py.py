import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

engine = create_engine('sqlite:////home/vincent/doop.sqlite')


from sqlalchemy import Column, Integer, String, Numeric, ForeignKey


class Store(Base):
	__tablename__ = 'Store'
	id = Column(Integer, primary_key=True)
	
	name = Column(String, nullable=False)


class Reciept(Base):
	__tablename__ = 'Reciept'
	id = Column(Integer, primary_key=True)
	
	store_id = Column(Integer, ForeignKey(Store.id), nullable=False)
	store = relationship(Store)


class PurchaseUnit(Base):
	__tablename__ = 'PurchaseUnit'
	
	id = Column(Integer, primary_key=True)
	
	name = Column(String, nullable=False)


class Purchase(Base):
	__tablename__ = 'Purchase'
	
	id = Column(Integer, primary_key=True)
	
	reciept_id = Column(Integer, ForeignKey('Reciept.id'),
		nullable=False)
	reciept = relationship("Reciept")
	
	description = Column(String, nullable=False)
	
	qty = Column(String, nullable=False)
	
	cost = Column(Integer, nullable=False)
	
	unit_id = Column(Integer, ForeignKey('PurchaseUnit.id'), nullable=False)
	unit = relationship("PurchaseUnit")
	
	def __repr__(self):
		return "{qty} {desc} @ {cost} [id: {id}, r: {rid}]".format(
			qty=self.qty, 
			desc=self.description, 
			cost=self.cost,
			id=self.id,
			rid=self.reciept)

Base.metadata.create_all(engine)

####

from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
session = Session()

s = Store(name="Apple Market")

r = Reciept(store=s)

u = PurchaseUnit(name="Oz")

session.add(r)
session.add(u)

def create(desc, qty, cost):
	
	p = Purchase(reciept=r, unit=u, description=desc, qty=qty, cost=cost)
	session.add(p)


create("Apples", 3, 2.3)

create("Bananas", 2, 0.39)

session.commit()

for p in session.query(Purchase):
	print (p)
	print (p.reciept.store.name)
	
for r in session.query(Store).filter(Store.name.like('%foo%')):
	print ('fo' + r.name)
	
"""
class Tag(Base):
	pass
"""
